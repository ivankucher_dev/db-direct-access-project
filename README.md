# Personal database project with direct access to the data

The main and hard issue was to made overflow protection. Database file is a simple txt file , main functions was created to work with him.
As we know , direct access working with blocks , to access virtual block we need to know the hash code , in my program its a simple hash code ( n%10 ).
In block we can have just 10 records , else block is overflowed, and program need to solve this collision.

Program implements the following functions:
1.1. Creating a database file.
1.2. Print the database.
1.3. Reading the record by entered key.
1.4. Search for a record in specified field.
1.5. Deleting a record by entered key.
1.6. Insert record.
1.7. Modificate record.
1.8. Overflow protection


---

## Screenshots
![Alt text](https://lh3.googleusercontent.com/KsC-IWyeJAVTaIyKFL8WN6vYWAyUoZ7SdRN0JShXWWJ_cHoRLC1SJMXSRgcBxDXEP4WVGuncaFtKrHxQjQIbGRbjbWripWoHKkdddk_oSV95b7PORUsLr9Gmww3uwMn01rlol-aZdoUKkSGI-5UeJ2Ort9ft5pDj3DOsv1XaQOfIMB7Z16kzcX_lVIbw0c1FsTOVOIZBp0QwNAtC_8bgMN4f4UYza41yI_UlFofQ7-RWdIs4s6lHRztLrbJkdKYPWWdxmapXVA6pUH-6JEoLiEKhM3NstUXLDBCMB6GB_R37zBrvIKXvLH77yANbxcBlQbpjkxSe-jtQiOf6a4syCJ8JnoAD7wvDS7GoZQizd5eYrtXjR_xVlxCjbwVcMDiewpAvKrXlaMgchleFT2Xd9ZOqA6VX9dtdTE0zXJYPVwIKDOME1RUT3JMzvq5_wUoIQMfDlADwTpq5sIxyX1l8_WoZkd7V_1AuXCtar05dQK9nNJKn9j1PcdqmdmaQ8y217X7x_XX89DUtjEkCteLLMjHCgqNPeoAKmmxrSoBjQOuB0PseXK10JObH1WvcvdzCV5NYqxGtX2vRCIesZU-SdZu0VAGIb4_Sym1-tEETT62Yws2uuLmdVmqDgYKxBalc7ia1ctMOYa2n8aVMDqWz0vsm6pGJET0=w406-h350-no)
![Alt text](https://lh3.googleusercontent.com/6h22AGcVMR7-jdMJIw17EdRHZu-r-TgZ-6g4xvJtQaqd5eRLRqjZ9gFfLTi3iXChjNTOTMtoZWenZdiokNIuMzsGB5gcCgVSybTVKJR3hAJYk2BdMVd2_3vBt_9eW3UOGXlio5NkfhVQejzkCD-GVHi3SGJhJsUxbKinKIQTk3HeEXt52W7MViEdpQTxM4sh8wfr9QLwwaeX5jmGh9_1Kw1JKpa7WrzbMbUKsGJKMA_m7c1nfTBO1dVY8eXw7XR8HOEYciZkRQv9kBZaM1MtUAUc9Fbx7dwjuQSx-WRhb9-k68vUAgtaZiKsdfkA_1sTZK9XjG9dpgs_bqYT3wss8_o3TR2AZR9zRPB7YmS1MaXptuNOWteCsu7-CCZz8NDT11gP6Mgj7jOtgBuQwGTaIK76fyfH-VgLzYuHH96IlmDqIovigy9VlnEwynyYA92Dk2ZGVYP8SwotDnGAMxq7UvBO_g9OsHQASf1hqdlCkDW4CY1I0RW-q0TuRyhRiSVelr5Iwp3qkRIorEXhyhLgZnpZy2Y8nK3h-HVXf6T6OD0Lqs0T3qwfiVm1jr67fGKmVmPtILmBZ1PUPhl1ricqrHBxhslpTYggmvgXL4Co-f4PCPNgMVW3bhm_KStupxZJjVzyCCFXiiqKmnr6SkBXk2m8eSvJc4I=w839-h540-no)

---