package com.iomis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;



public class DBHelper {

   boolean availability,discount;
     int id,amount,price;
     int n=10;
     long barcode;
    float weight;
    String ProductName,location,ProductType;
    String DBName,DBPath;
    File dbfile;


    public String readInputFields() {
       Scanner scanner = new Scanner(System.in);

        System.out.println("\n-----Вставка запису-----\n");
        System.out.println("\nВведіть ID товару : ");

            boolean isntExists;
            //перевірити чи існує такий ID
            do{
                isntExists=false;
                this.id=scanner.nextInt();
                this.id=Math.abs(id);
                isntExists = checkIdAlreadyExists(id);
                if(!isntExists)
                    System.out.println("Такий ID вже існує!Спробуйте ще раз");
            }while (!isntExists);

            scanner.nextLine();
        System.out.print("Введіть назву товару : ");

            this.ProductName=scanner.nextLine();

        System.out.print("Введіть вид товару : ");

            this.ProductType=scanner.nextLine();


        System.out.print("Введіть наявність товару на складі(y-так/n-ні) : ");
        String strAvalibility;
        do {
            strAvalibility = scanner.nextLine();
            if(strAvalibility=="y")
                availability=true;
            else if(strAvalibility=="n")
                    availability=false;

        }while(strAvalibility=="y" || strAvalibility=="n");


        System.out.print("Введіть ціну : ");

            this.price=scanner.nextInt();

        System.out.print("Введіть кількість товару: ");


            this.amount=scanner.nextInt();



        System.out.print("Введіть вагу : ");

            this.weight=scanner.nextFloat();


        System.out.print("Введіть штрих-код(не меньше 12 символів) : ");
        String code;
do {
    this.barcode = scanner.nextLong();
     code = String.valueOf(barcode);
     if(code.length()<12)
         Printer.ErrorInput();
}while (code.length()<12);

        scanner.nextLine();
        System.out.print("Введіть локацію магазину 'Ельдорадо' : ");

            this.location=scanner.nextLine();

        System.out.print("Введіть наявність знижки на даний товар (y-так/n-ні): ");
        String strDiscount;
        do {
            strDiscount = scanner.nextLine();
            if(strDiscount=="y")
                this.discount=true;
            else if(strDiscount=="n")
                this.discount=false;


        }while(strDiscount=="y" || strDiscount=="n");


       String result = this.id +"," +this.ProductName +"," +this.ProductType +"," +this.availability +"," +this.price +"," +this.amount +"," +this.weight +"," +this.barcode +"," +this.location +"," +this.discount;
       return result;


    }



    public String readInputFields(int ID) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("\n-----Редагування  запису-----\n");

        System.out.print("Введіть назву товару : ");

        this.ProductName=scanner.nextLine();

        System.out.print("Введіть вид товару : ");

        this.ProductType=scanner.nextLine();


        System.out.print("Введіть наявність товару на складі(y-так/n-ні) : ");
        String strAvalibility;
        do {
            strAvalibility = scanner.nextLine();
            if(strAvalibility=="y")
                availability=true;
            else if(strAvalibility=="n")
                availability=false;

        }while(strAvalibility=="y" || strAvalibility=="n");


        System.out.print("Введіть ціну : ");

        this.price=scanner.nextInt();

        System.out.print("Введіть кількість товару: ");


        this.amount=scanner.nextInt();



        System.out.print("Введіть вагу : ");

        this.weight=scanner.nextFloat();


        System.out.print("Введіть штрих-код(не меньше 12 символів) : ");
        String code;
        do {
            this.barcode = scanner.nextLong();
            code = String.valueOf(barcode);
            if(code.length()<12)
                Printer.ErrorInput();
        }while (code.length()<12);

        scanner.nextLine();
        System.out.print("Введіть локацію магазину 'Ельдорадо' : ");

        this.location=scanner.nextLine();

        System.out.print("Введіть наявність знижки на даний товар (y-так/n-ні): ");
        String strDiscount;
        do {
            strDiscount = scanner.nextLine();
            if(strDiscount=="y")
                this.discount=true;
            else if(strDiscount=="n")
                this.discount=false;


        }while(strDiscount=="y" || strDiscount=="n");


        String result =ID +"," +this.ProductName +"," +this.ProductType +"," +this.availability +"," +this.price +"," +this.amount +"," +this.weight +"," +this.barcode +"," +this.location +"," +this.discount;
        return result;


    }



    public void scanFileData(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введіть шлях до файлу: ");
        DBPath = scanner.nextLine();
        System.out.print("Імя файлу(без розрширення файлу) : ");
        DBName = scanner.nextLine()+".txt";
    }

    //Функція для перевірки чи введений новий ID вже інсує
    //Функція для перевірки чи введений новий ID вже інсує
    public boolean checkIdAlreadyExists(int id){
        ArrayList <String> infoFromFile = FileWorker.read(new File(DBPath,DBName).getAbsolutePath());
        String strID;
        strID=Integer.toString(id);
        strID.trim();
        for(int i=0;i<infoFromFile.size();i++){
            String s;
            s=infoFromFile.get(i);
            //Дістаємо з бази даних ID який є 0 елементом
            s=s.split(",")[0];

            if(s.equals(strID)){
                return false;
            }


        }
        return true;
    }



    public boolean checkFileForExists(){
        //Якщо такий шлях існує
        if(new File(DBPath).exists()){
            //Якщо такого файлу ще не створено
            if(!new File(DBPath,DBName).exists()){
                return true;
            }
            else
            {
                Printer.fileExists();
                int choise = Menu.getInput(1,2);
                switch (choise){
                    case 1:
                        overwriteFile();
                        System.out.println("Успішно перезаписано");
                        return true;

                    case 2:
                        return false;
                }

            }

        }else{
            Printer.DirectoryErrorMessage();
            return false;
        }
        return false;
    }


    public boolean OpenFileCheckErrors(){
        //Якщо такий шлях існує
        if(new File(DBPath).exists()){
            //Якщо такого файлу ще не створено
            if(new File(DBPath,DBName).exists()){
                //проблем немає, відкриваємо файл
                Printer.DBSuccessfullyOpenedMessage();
                return true;
            }
            else Printer.fileDoesntExists();

        }else{
            Printer.DirectoryErrorMessage();
            return false;
        }
        return false;
    }


    //Метод для перезапису файлу бд якщо такий вже існує
    public void overwriteFile(){
        //відкриваємо файл
        File dbfile = new File(DBPath, DBName);

        dbfile.delete();
        try
        {
            //якщо можемо створити файл бд, створємо його
            boolean created = dbfile.createNewFile();

        }
        catch(IOException ex){
            System.out.println(ex.getMessage());

        }

    }


    //Метод для видалення файлу
    public void delete(){
        //відкриваємо файл
        File dbfile = new File(DBPath, DBName);

        dbfile.delete();


    }




    public File createOutputFile(){
        File outputFile = new File(DBPath,"outputFile.txt");

        try
        {
            //якщо можемо створити файл бд, створємо його
            boolean created = outputFile.createNewFile();

        }
        catch(IOException ex){
            System.out.println(ex.getMessage());

        }

        return outputFile;

    }


    //Метод для створення нового файлу бази даних
    public File createFile(){
        dbfile = new File(DBPath, DBName);
        //Пробуємо створити файл за шляхом та назвою
        try
        {
            boolean created = dbfile.createNewFile();
            for(int i=0;i<12;i++) {
                FileWorker.write(dbfile.getAbsolutePath(), "+");
                FileWorker.write(dbfile.getAbsolutePath(), "");

            }

            for(int i=0;i<2;i++) {
                FileWorker.write(dbfile.getAbsolutePath(), "-");
                FileWorker.write(dbfile.getAbsolutePath(), "");

            }
            if(created)
                System.out.println("База даних успішно створена");
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());


        }

        return dbfile;


    }




    public int h(int id){

     return id%n;

    }


}
