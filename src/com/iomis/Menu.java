package com.iomis;

import java.util.*;

public class Menu {

    public static int getInput(int firstLimit, int secondLimit){
        Scanner scanner = new Scanner(System.in);
        int choise=-1;
        while (choise<firstLimit || choise>secondLimit){
            try{
                System.out.println("Введіть ваш вибір : ");
                choise = Integer.parseInt(scanner.nextLine());
            }
            catch (NumberFormatException e){
                System.out.println("Неправильний ввід! Спробуйте знову");
            }
        }
        return choise;
    }




    public void MultiLevelMenu() throws Exception{

        int input,secondStage,thirdStage;
        boolean isCreated=false,isOpened=false;
        DBManager db = new DBManager();
        DBGroupManager GroupDB = new DBGroupManager();
        Printer.printMainMenu();

        input=Menu.getInput(1,2);
        //Перша стадія головного меню
        switch (input){
            case 1:
                //Вибрано меню для роботи з файлами бази данних
                Printer.printMenuWorkWithFiles();
                secondStage=Menu.getInput(1,3);
                switch (secondStage){
                    //Вибрано меню відкрити існуючу базу данних
                    case 1:
                        while(!isOpened) { isOpened = db.openDB(); }

                            //Будемо працювати з базою даних до тих пір поки користувач не захоче вийти(натиснута кнопка 7)
                            do {
                                //База банних відкрита успішно - переходимо до основної стадії(робота безпосередньо з бд)
                                Printer.printMenuWorkWithDB();
                                thirdStage = getInput(1, 7);

                                switch (thirdStage) {
                                    case 1:
                                        db.select();
                                        System.out.println("\nНатисніть 1 щоб продовжити роботу з БД....");
                                        input=getInput(1,1);
                                        while(input!=1){}
                                        break;

                                    case 2:
                                        db.selectByID();
                                        break;
                                    case 3:
                                        db.selectByField();
                                        break;
                                    case 4:
                                        db.insert();
                                        break;
                                    case 5:
                                        db.delete();
                                        break;
                                    case 6:
                                        db.modify();
                                        break;

                                }
                            }while(thirdStage!=7);
                            //Закриваємо всі потоки

                        break;


                    case 2:
                        //Вибрано меню створити нову базу данних


                        while(!isCreated) {
                            isCreated= db.createNewDB();
                        }

                        //Будемо працювати з базою даних до тих пір поки користувач не захоче вийти(натиснута кнопка 7)
                        do {
                            //База банних створена успішно - переходимо до основної стадії(робота безпосередньо з бд)
                            Printer.printMenuWorkWithDB();
                            thirdStage = getInput(1, 7);

                            switch (thirdStage) {
                                case 1:
                                    db.select();
                                    System.out.println("\nНатисніть 1 щоб продовжити роботу з БД....");
                                    input=getInput(1,1);
                                    while(input!=1){}
                                    break;

                                case 2:
                                    break;
                                case 3:
                                    break;
                                case 4:
                                    db.insert();
                                    break;
                                case 5:
                                    db.delete();
                                    break;
                                case 6:
                                    db.modify();
                                    break;

                            }
                        }while(thirdStage!=7);

                        break;

                        //Вихід з програми
                    case 3:
                        System.exit(1);
                }
                break;







            case 2:
                //вихід з програми
                System.exit(1);
                break;

        }
    }

}
