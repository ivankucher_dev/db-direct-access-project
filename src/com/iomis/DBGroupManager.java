package com.iomis;


import java.io.*;
import java.util.*;

public class DBGroupManager {

    private Scanner scanner;
    private BufferedReader br;
    private BufferedWriter bw;

    private int input;
    private boolean isProblem=false;
    private DBHelper dbhelper;
    private File dbFile;
    private ArrayList<String> insertLines;
    private ArrayList<String> modifyLines;
    public static Map<Integer,String> GroupMap ;
    private ArrayList<Integer> keys;
    private Iterator CommandString;



    public DBGroupManager(){
        this.dbhelper = new DBHelper();
        GroupMap = new LinkedHashMap<Integer, String>();
        insertLines = new ArrayList<>();
        modifyLines = new ArrayList<>();
        keys = new ArrayList<>();

    }




    //Метод для створення нового файлу бази даних
    public boolean createNewDB() throws Exception {
        scanner = new Scanner(System.in);

//Зчитуємо шлях до файлу та назву файлу
        do {
            dbhelper.scanFileData();
            //поки є помилки - зчитуємо дані
        }while (!dbhelper.checkFileForExists());

        dbhelper.createFile();

        //готуємо файл до запису та читання , відкриваємо буфери зчитування та запису



        return true;
    }




    public boolean openDB() throws Exception{
        System.out.println("---Відкриття файлу бази даних---\n");

//Зчитуємо шлях до файлу та назву файлу для відкриття
        do {
            dbhelper.scanFileData();
            //поки є помилки - зчитуємо дані
        }while (!dbhelper.OpenFileCheckErrors());


        //готуємо файл до запису за читання , відкриваємо буфери зчитування та запису



        return true;
    }







    public void select(){
        int i=0;
        String line;
        while(!FileWorker.read(getFilePath(),i).equals("nolines")){
            line=FileWorker.read(getFilePath(),i);
            String[]s = line.split(",");
            System.out.format(String.format(Printer.format, (Object[]) s));
            i++;
            line="";

        }


    }




    //Перша стаді перед вставкою (дія додається в масив у вигляді ключ-значення , якщо така існує , ми перезаписуємо її, тобто беремо останню дію)

    public void GroupInsert(){
        //зчитуємо вхідні дані
        String inputStr =dbhelper.readInputFields();
        String inputID=inputStr.split(",")[0];
        String lineID;
        inputID.trim();
        int id=Integer.parseInt(inputID);
        int i=0,idOfLine;
        boolean inserted=false;
        //потім порівнюємо чи були введені вже такі вхідні дані, якщо так, переписуємо їх
        while(insertLines.size()!=i) {
            lineID = insertLines.get(i).split(",")[0];
            lineID.trim();
            idOfLine = Integer.parseInt(lineID);
            if(id==idOfLine) {
                replaceLineInArray(id, insertLines, inputStr);
                inserted=true;
                break;
            }
           i++;

        }
        //якщо ми не знайшли дві однакові дані , то просто вносимо їх
        if(inserted==false){
            insertLines.add(inputStr);
        }
        //Додаємо наше ключ-значення
        keys.add(id);
        GroupMap.put(id,"insert");

    }


    //Перша стаді перед видаленням (дія додається у вигляді ключ-значення , якщо така існує , ми перезаписуємо її, тобто беремо останню дію)

    public void GroupDelete(){
        scanner = new Scanner(System.in);
        int id=-1;

        System.out.println("\nВведіть ID товару : ");


            id=scanner.nextInt();
            id=Math.abs(id);

        GroupMap.put(id,"delete");

        /*for (int key : GroupMap.keySet()) {
            System.out.println(key);
        }

        for (String key : GroupMap.values()) {
            System.out.println(key);
        }*/



    }

    public void GroupModify(){
        int id;
        //Зчитуємо ID
        scanner = new Scanner(System.in);

        System.out.println("\nВведіть ID товару, який потрібно модифікувати : ");


            id=scanner.nextInt();
            id=Math.abs(id);


        String inputNewData = dbhelper.readInputFields(id);
        String lineID;
        int i=0,idOfLine;
        boolean inserted=false;
        while(modifyLines.size()!=i) {
            lineID = modifyLines.get(i).split(",")[0];
            lineID.trim();
            idOfLine = Integer.parseInt(lineID);
            if(id==idOfLine) {
                replaceLineInArray(id, modifyLines, inputNewData);
                inserted=true;
                break;
            }
            i++;

        }
        if(inserted==false){
            modifyLines.add(inputNewData);
        }

        GroupMap.put(id,"modify");

    }



    public String findInputString(int id,ArrayList<String> Lines){
        int lineID;
        for(int i=0;i<Lines.size();i++){
            lineID = Integer.parseInt(Lines.get(i).split(",")[0]);
            if(lineID==id){
                return Lines.get(i);
            }
        }
        return "nolines" ;
    }


    //Заміна запису в масиві ( Якщо такий запис вже існує, заміняємо його , для уникнення помилок)
    public void replaceLineInArray(int id,ArrayList<String> Lines,String replaceElement){
        int lineID;
        for(int i=0;i<Lines.size();i++){
            lineID = Integer.parseInt(Lines.get(i).split(",")[0]);
            System.out.println("lineID+ "+lineID);
            if(lineID==id){
              Lines.set(i,replaceElement);
            }
        }
    }



//Сам алгоритм групової обробки ( Основний елемент робити з БД)

    public void DBAlgorithm(){

InitializeString();
      String LineToPut;

        while (CommandString.hasNext()) {

            Map.Entry pair = (Map.Entry)CommandString.next();
            String str = pair.getValue().toString();
            switch (str){
                case "insert" :
                    //Використовуємо раніше створену функцію , знаходимо вхідну стрічку і поміщаємо її для вставки в створену функцію insert
                    LineToPut = findInputString(Integer.parseInt(pair.getKey().toString()),insertLines);
                    insert(Integer.parseInt(pair.getKey().toString()),LineToPut);
                    break;
                //Використовуємо раніше створену функцію , поміщаємо її для видалення
                case "delete" :
                    delete(Integer.parseInt(pair.getKey().toString()));
                    break;
                //Використовуємо раніше створену функцію , знаходимо вхідну стрічку і поміщаємо її для редагування запису в створену функцію modify
                case "modify" :
                    LineToPut = findInputString(Integer.parseInt(pair.getKey().toString()),modifyLines);
                    int insertkey = Integer.parseInt(LineToPut.split(",")[0]);
                    int IDinFileFound = findIDinFile(insertkey);
                    for(int key: keys){
                        if(insertkey==key ){
                            modify(insertkey,LineToPut);
                        }

                    }
                    if(IDinFileFound==1){
                        modify(insertkey,LineToPut);

                    }

                    break;
            }
            //Переходимо до насступної команди
            CommandString.remove();
        }



    }


  public void InitializeString(){
      CommandString = GroupMap.entrySet().iterator();
  }









    public void insert(int id,String inputStr){


        int lineID;
        int i=0;
        String line;

        File outputFile =  dbhelper.createOutputFile();

        boolean inserted =false;
        while(true){

            //зчитуємо однин запис з файлу з індексом і
            line=FileWorker.read(getFilePath(),i);
            //якщо це останній запис і вставка ще не відбулась то вставляємо запис і виходимо з циклу
            if(line.equals("nolines") && inserted==false){
                FileWorker.write(getOutputFilePath(), inputStr);
                break;
            }
            //інакше якщо це просто кінець файлу по виходимо з циклу
            else if (line.equals("nolines")) {

                break;
            }
            //дістали ID запису і порівнюємо його з іншими ID для сортування
            String IDline = line.split(",")[0];
            IDline.trim();
            lineID=Integer.parseInt(IDline);

            //якщо ід запису у файлі більше ід запису який ми намагаємось вставити і вставка ще не відбулась
            if(lineID>id && inserted==false) {
                //спочатку записуємо стрічку яка вводилась , потім запис який був у файлі
                FileWorker.write(getOutputFilePath(), inputStr);
                FileWorker.write(getOutputFilePath(), line);
                inserted=true;

            }

            //інакше якщо це не кінець файлу то просто вставляємо запис який вже був у файлі
            else if(!FileWorker.read(getFilePath(),i).equals("nolines")){
                FileWorker.write(getOutputFilePath(),line);
            }

            //збільшуємо індекс та занулюємо стрічку
            i++;
            line="";

        }


        //видаляємо наш старий файл та переназиваємо новий файл у старий
        dbhelper.delete();
        outputFile.renameTo(new File(dbhelper.DBPath,dbhelper.DBName));


        System.out.println("---------------Запис успішно виконано!-------------------\n");
    }


    public int modify(int newID,String inputNewData) {

        File outputFile =  dbhelper.createOutputFile();
        String line;
        int j=0,i=0, index=-1,lineID;
        boolean inserted=false;



        while(true) {


            //зчитуємо однин запис з файлу з індексом j
            line = FileWorker.read(getFilePath(), j);
            if(line.equals("nolines")){

                break;
            }
            //дістали ID запису і порівнюємо його з іншими ID для сортування
            String IDline = line.split(",")[0];
            IDline.trim();
            lineID = Integer.parseInt(IDline);

            //якщо рівне - запамятовуємо індекс
            if (newID==lineID) {
inserted=true;
                break;
            }


            j++;

        }



        if(inserted==false){
            insert(newID,inputNewData);
            System.out.println("---------------Успішно відредаговано!-------------------\n");
          return 0;
        }
        while(true){

            line = FileWorker.read(getFilePath(), i);
            if(line.equals("nolines")){
                break;
            }

            if(i==j){
                FileWorker.write(getOutputFilePath(), inputNewData);
            }
            else {
                FileWorker.write(getOutputFilePath(), line);
            }
            i++;
        }

        dbhelper.delete();
        outputFile.renameTo(new File(dbhelper.DBPath,dbhelper.DBName));

        System.out.println("---------------Успішно відредаговано!-------------------\n");
        return 1;
    }



    public int findIDinFile(int id){
        String line;
        int j=0,lineID;
        while(true) {


            //зчитуємо однин запис з файлу з індексом j
            line = FileWorker.read(getFilePath(), j);
            if(line.equals("nolines")){

                break;
            }
            //дістали ID запису і порівнюємо його з іншими ID для сортування
            String IDline = line.split(",")[0];
            IDline.trim();
            lineID = Integer.parseInt(IDline);

            //якщо рівне - запамятовуємо індекс
            if (id==lineID) {
                System.out.println("found ID");
                return 1;
            }


            j++;

        }

return 0;
    }





    public void delete(int id){

        File outputFile =  dbhelper.createOutputFile();
        //Зчитуємо ID
        scanner = new Scanner(System.in);
        int lineID,i=0;
        String line;


        while(true) {

            //зчитуємо однин запис з файлу з індексом j
            line = FileWorker.read(getFilePath(), i);
            if(line.equals("nolines")){
                break;
            }


            //дістали ID запису і порівнюємо його з іншими ID для сортування
            String IDline = line.split(",")[0];
            IDline.trim();
            lineID = Integer.parseInt(IDline);

            //якщо рівне - запамятовуємо індекс
            if (lineID==id) {
                i++;
            }
            else{
                FileWorker.write(getOutputFilePath(), line);
                i++;
            }


        }

        dbhelper.delete();
        outputFile.renameTo(new File(dbhelper.DBPath,dbhelper.DBName));

        System.out.println("---------------Успішно видалено!-------------------\n");

    }

    public void selectByID(){


        String line;
        int j=0,lineID,newID=-1;

        //Зчитуємо ID
        scanner = new Scanner(System.in);

        System.out.println("\nВведіть ID товару, для читання : ");

        boolean isntExists;
        //перевірити чи існує такий ID
        do{
            isntExists=false;
            newID=scanner.nextInt();
            newID=Math.abs(newID);
            //Перевіряємо чи такий ID існує
            isntExists = dbhelper.checkIdAlreadyExists(newID);
            if(isntExists)
                System.out.println("Такого ID не існує!Спробуйте ще раз");
        }while (isntExists);

        //Зчитуємо всі дані з файлу




        while(true) {

            //зчитуємо однин запис з файлу з індексом j
            line = FileWorker.read(getFilePath(), j);


            //дістали ID запису і порівнюємо його з іншими ID для сортування
            String IDline = line.split(",")[0];
            IDline.trim();
            lineID = Integer.parseInt(IDline);

            //якщо рівне - запамятовуємо індекс
            if (newID==lineID) {
                String[]s = line.split(",");
                System.out.format(String.format(Printer.format, (Object[]) s));
                break;
            }

            j++;

        }



        System.out.println("\n---------------Запис зчитано!-------------------\n");

    }


    public void selectByField(){


        String line,fieldLine;
        int j=0,lineID,newID=-1;
        int IndexOffield;
        int counter=0;

        //Зчитуємо ID
        scanner = new Scanner(System.in);


        Printer.printChooseField();
        System.out.println("\nВведіть номер поля , яке потрібно зчитати : ");
        IndexOffield=Menu.getInput(1,10);
        System.out.println("\nВведіть значення поля , яке потрібно зчитати : ");
        fieldLine=scanner.nextLine();

        while(true) {

            //зчитуємо однин запис з файлу з індексом j
            line = FileWorker.read(getFilePath(), j);
            if(line.equals("nolines")){
                System.out.println("\n\n@@@@@@@@   ЗНАЙДЕНО "+counter+" ЗАПИСІВ    @@@@@@@@");
                break;
            }

            //дістали ID запису і порівнюємо його з іншими ID для сортування
            String FieldFromFile = line.split(",")[IndexOffield-1];
            FieldFromFile.trim();

            //якщо рівне - запамятовуємо індекс
            if (FieldFromFile.equals(fieldLine)) {
                String[]s = line.split(",");
                System.out.format(String.format(Printer.format, (Object[]) s));
                counter++;
            }

            j++;

        }


    }


    public  String getFilePath(){
        return (new File(dbhelper.DBPath, dbhelper.DBName).getAbsolutePath());
    }

    public String getOutputFilePath(){
        return (new File(dbhelper.DBPath,"outputFile.txt").getAbsolutePath());
    }

}












