package com.iomis;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileWorker {

    public static void write(String path, String text) {

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path, true));
            writer.append(text);
            writer.append('\n');
            writer.close();



        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static ArrayList<String> read(String path) {
        //Обект для побудови String
        ArrayList<String> record = new ArrayList<>();


        try {
            //Читаємо файл в буфер
            BufferedReader in = new BufferedReader(new FileReader(path));
            try {
                //Зчитуємо пострічно
                String s;
                while ((s = in.readLine()) != null) {
                    record.add(s);

                }
            } finally {
                in.close();
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        }

        return record;
    }

    public static String read(String path,int line) {
        //Обект для побудови String
        String record=null;

        int linecounter=0;
        try {
            //Читаємо файл в буфер
            BufferedReader in = new BufferedReader(new FileReader(path));
            try {
                //Зчитуємо пострічно
                String s;
                while ((s = in.readLine()) != null) {
                    record=s;
                    if(line==linecounter)
                        return record;

                    linecounter++;

                }
            } finally {
                in.close();
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        }

        return "nolines";
    }


    public void reWrite(String DBPath, String DBName){

    }

}
