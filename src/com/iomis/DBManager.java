package com.iomis;

import jdk.nashorn.internal.ir.Block;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class DBManager {

   private Scanner scanner;
   private BufferedReader br;
   private BufferedWriter bw;

   private int input;
   private boolean isProblem=false;
    private DBHelper dbhelper;
private File dbFile;


   public DBManager(){
       this.dbhelper = new DBHelper();
   }




   //Метод для створення нового файлу бази даних
    public boolean createNewDB() throws Exception {
scanner = new Scanner(System.in);

//Зчитуємо шлях до файлу та назву файлу
do {
   dbhelper.scanFileData();
   //поки є помилки - зчитуємо дані
}while (!dbhelper.checkFileForExists());

        dbhelper.createFile();

        //готуємо файл до запису та читання , відкриваємо буфери зчитування та запису



        return true;
        }




        public boolean openDB() throws Exception{
            System.out.println("---Відкриття файлу бази даних---\n");

//Зчитуємо шлях до файлу та назву файлу для відкриття
            do {
                dbhelper.scanFileData();
                //поки є помилки - зчитуємо дані
            }while (!dbhelper.OpenFileCheckErrors());


            //готуємо файл до запису за читання , відкриваємо буфери зчитування та запису



            return true;
        }






   /* public void select() throws Exception{


       //Зчитуємо файл( передаємо параметром шлях до файлу)
ArrayList<String> printdb = FileWorker.read(getFilePath());
for(int i=0;i<printdb.size();i++) {
    //Розбиваємо його на масив стрічок для виводу
    String[]s = printdb.get(i).split(",");
    //Виводимо на екран з допомогою створеного формату в класі Printer
    System.out.format(String.format(Printer.format, (Object[]) s));

}


    }*/

   public void select(){
       int i=0;
       String line;
       while(!FileWorker.read(getFilePath(),i).equals("nolines") && !FileWorker.read(getFilePath(),i+1).equals("nolines")){
           line=FileWorker.read(getFilePath(),i);
           String[]s = line.split(",");
           if(line.equals("+") || line.equals("-")){
               System.out.println("");
           }
           if(line.equals("+") || line.equals("-") || line.equals("")) {
               i++;
               continue;
           }
           System.out.format(String.format(Printer.format, (Object[]) s));
           i++;
           line="";

       }


   }


 /*   public void insert(){

        ArrayList<String> dbData = FileWorker.read(getFilePath());
        String inputStr =dbhelper.readInputFields();

        String s=inputStr.split(",")[0];
        s.trim();
        int id=Integer.parseInt(s);
        System.out.println("OUR ID "+ id);


        int index=sort(id);
        System.out.println("OUR index "+ index);
        dbhelper.overwriteFile();

        if(dbData.size()==0){
            FileWorker.write(getFilePath(),inputStr);
        }
        for(int i=0;i<dbData.size();i++){

            //Якщо ми знайшли перше ID більше ID ніж яке ми вставляємо і це не остання стрічка, то вставляємо нашу стрічку а потім встаєлямо наступні
            if(i==index && i!=dbData.size()-1){
                FileWorker.write(getFilePath(),inputStr);
                FileWorker.write(getFilePath(),dbData.get(i));

            }
            // інакше просто вставляємо стрічку яка вже була до того
            else if(i!=dbData.size()-1 || (i==dbData.size()-1 && index!=dbData.size()-1)) {
                FileWorker.write(getFilePath(),dbData.get(i));
            }


            //якщо це остання стрічка то вставляємо навпаки, нову стрічку в самий кінець
           if(i==dbData.size()-1 && index==dbData.size()-1){
                FileWorker.write(getFilePath(),dbData.get(i));
                FileWorker.write(getFilePath(),inputStr);
                break;
            }








        }
        System.out.println("---------------Запис успішно виконано!-------------------\n");
    }

*/


 public Boolean checkBlockOverflow(int block,int id,String inputStr,File outputFile) {
     int i = 0,counter=0,overflow=0;
     String line;
     int lineID;
     boolean inserted = false;
     boolean BlockOverflow = false;

     while (true) {

         //зчитуємо однин запис з файлу з індексом і
         line = FileWorker.read(getFilePath(), i);

         if(line.equals("nolines") && FileWorker.read(getFilePath(),i++).equals("nolines")){
             break;
         }

         if(line.equals("+")){
             counter++;
             i++;
             continue;

         }

         if(counter==block+1){

                 overflow++;

         }
i++;
     }
     i=0;

     if(overflow==dbhelper.n){
//Якщо переповнення то записати в блок переповнення

         while(true){

             //зчитуємо однин запис з файлу з індексом і
             line=FileWorker.read(getFilePath(),i);
             if(line.equals("nolines") && FileWorker.read(getFilePath(),i+1).equals("nolines")){
                 break;
             }
             if(line.trim().equals("-")){
                 BlockOverflow = true;
                 i++;
                 FileWorker.write(getOutputFilePath(), line);
                 continue;
             }

             if(BlockOverflow && inserted==false){

                 //якщо це останній запис і вставка ще не відбулась то вставляємо запис і виходимо з циклу
                 if(inserted==false && line.equals("") ){
                     FileWorker.write(getOutputFilePath(), inputStr);
                     inserted=true;

                 }
                 //інакше якщо це просто кінець файлу по виходимо з циклу
                 else {
                     //дістали ID запису і порівнюємо його з іншими ID для сортування
                     String IDline = line.split(",")[0];
                     IDline.trim();
                     lineID = Integer.parseInt(IDline);


                     //якщо ід запису у файлі більше ід запису який ми намагаємось вставити і вставка ще не відбулась
                     if (lineID > id && inserted == false) {
                         //спочатку записуємо стрічку яка вводилась , потім запис який був у файлі
                         FileWorker.write(getOutputFilePath(), inputStr);
                         FileWorker.write(getOutputFilePath(), line);
                         inserted = true;


                     }


                     else if(inserted==false && FileWorker.read(getFilePath(),i+1).equals("-")){
                         FileWorker.write(getOutputFilePath(), line);
                         FileWorker.write(getOutputFilePath(), inputStr);
                         inserted=true;
                     }

                     else{
                         FileWorker.write(getOutputFilePath(), line);
                     }


                 }


                 //збільшуємо індекс та занулюємо стрічку


             }
             else{
                 FileWorker.write(getOutputFilePath(), line);
             }

             i++;
             line="";


         }

         //видаляємо наш старий файл та переназиваємо новий файл у старий
         dbhelper.delete();
         outputFile.renameTo(new File(dbhelper.DBPath,dbhelper.DBName));


         System.out.println("---------------Запис успішно виконано!-------------------\n");
         return true;
     }


     return false;
 }









    public Boolean checkBlockOverflow(int block) {
        int i = 0, counter = 0, overflow = 0;
        String line;
        int lineID;

        boolean inserted = false;
        boolean BlockOverflow = false;

        while (true) {

            //зчитуємо однин запис з файлу з індексом і
            line = FileWorker.read(getFilePath(), i);

            if (line.equals("nolines") && FileWorker.read(getFilePath(), i++).equals("nolines")) {
                break;
            }

            if (line.equals("+")) {
                counter++;
                i++;
                continue;

            }

            if (counter == block + 1) {

                overflow++;

            }
            i++;
        }
        i = 0;

        if (overflow == dbhelper.n)
return true;
        else
            return false;

    }












    public void insert(){


        String inputStr =dbhelper.readInputFields();

        String inputID=inputStr.split(",")[0];
        inputID.trim();

        int id=Integer.parseInt(inputID);
        int lineID;
        int i=0;

        String line;
        String separator;
        int block=0,counter=0;

   File outputFile =  dbhelper.createOutputFile();
  block=dbhelper.h(id);



        boolean inserted =false;
   boolean Overflow = checkBlockOverflow(block,id,inputStr,outputFile);
   if(Overflow==true)
       return;

        while(true){

            //зчитуємо однин запис з файлу з індексом і
            line=FileWorker.read(getFilePath(),i);
            if(line.equals("nolines") && FileWorker.read(getFilePath(),i+1).equals("nolines")){
                break;
            }
            if(line.equals("+")){
                counter++;
                i++;
                FileWorker.write(getOutputFilePath(), line);
                continue;
            }

            if(counter==block+1 && inserted==false){

                //якщо це останній запис і вставка ще не відбулась то вставляємо запис і виходимо з циклу
                if(inserted==false && line.equals("") ){
                    FileWorker.write(getOutputFilePath(), inputStr);
                    inserted=true;

                }
                //інакше якщо це просто кінець файлу по виходимо з циклу
                else {
                    //дістали ID запису і порівнюємо його з іншими ID для сортування
                    String IDline = line.split(",")[0];
                    IDline.trim();
                    lineID = Integer.parseInt(IDline);


                    //якщо ід запису у файлі більше ід запису який ми намагаємось вставити і вставка ще не відбулась
                    if (lineID > id && inserted == false) {
                        //спочатку записуємо стрічку яка вводилась , потім запис який був у файлі
                        FileWorker.write(getOutputFilePath(), inputStr);
                        FileWorker.write(getOutputFilePath(), line);
                        inserted = true;


                    }


                   else if(inserted==false && FileWorker.read(getFilePath(),i+1).equals("+")){
                        FileWorker.write(getOutputFilePath(), line);
                        FileWorker.write(getOutputFilePath(), inputStr);
                        inserted=true;
                    }

                    else{
                        FileWorker.write(getOutputFilePath(), line);
                    }


                }


                //збільшуємо індекс та занулюємо стрічку


            }
            else{
                FileWorker.write(getOutputFilePath(), line);
            }

            i++;
            line="";


        }




        //видаляємо наш старий файл та переназиваємо новий файл у старий
        dbhelper.delete();
        outputFile.renameTo(new File(dbhelper.DBPath,dbhelper.DBName));


        System.out.println("---------------Запис успішно виконано!-------------------\n");
    }












    public void insert(String insertLine){


        String inputStr=insertLine;
String line;
        String inputID=inputStr.split(",")[0];
        inputID.trim();

        int id=Integer.parseInt(inputID);
        int lineID;
        int i=0;


        int block=0,counter=0;

        File outputFile =  dbhelper.createOutputFile();
        block=dbhelper.h(id);



        boolean inserted =false;


        while(true){

            //зчитуємо однин запис з файлу з індексом і
            line=FileWorker.read(getFilePath(),i);
            if(line.equals("nolines") && FileWorker.read(getFilePath(),i+1).equals("nolines")){
                break;
            }
            if(line.equals("+")){
                counter++;
                i++;
                FileWorker.write(getOutputFilePath(), line);
                continue;
            }

            if(counter==block+1 && inserted==false){

                //якщо це останній запис і вставка ще не відбулась то вставляємо запис і виходимо з циклу
                if(inserted==false && line.equals("") ){
                    FileWorker.write(getOutputFilePath(), inputStr);
                    inserted=true;

                }
                //інакше якщо це просто кінець файлу по виходимо з циклу
                else {
                    //дістали ID запису і порівнюємо його з іншими ID для сортування
                    String IDline = line.split(",")[0];
                    IDline.trim();
                    lineID = Integer.parseInt(IDline);


                    //якщо ід запису у файлі більше ід запису який ми намагаємось вставити і вставка ще не відбулась
                    if (lineID > id && inserted == false) {
                        //спочатку записуємо стрічку яка вводилась , потім запис який був у файлі
                        FileWorker.write(getOutputFilePath(), inputStr);
                        FileWorker.write(getOutputFilePath(), line);
                        inserted = true;


                    }


                    else if(inserted==false && FileWorker.read(getFilePath(),i+1).equals("+")){
                        FileWorker.write(getOutputFilePath(), line);
                        FileWorker.write(getOutputFilePath(), inputStr);
                        inserted=true;
                    }

                    else{
                        FileWorker.write(getOutputFilePath(), line);
                    }


                }


                //збільшуємо індекс та занулюємо стрічку


            }
            else{
                FileWorker.write(getOutputFilePath(), line);
            }

            i++;
            line="";


        }




        //видаляємо наш старий файл та переназиваємо новий файл у старий
        dbhelper.delete();
        outputFile.renameTo(new File(dbhelper.DBPath,dbhelper.DBName));
        System.out.println("----------Успішно видалено!!!!!--------------");
return;
    }










    public void modify() {

        File outputFile =  dbhelper.createOutputFile();
        String line;
        int j=0,i=0, index=-1,lineID,newID=-1;

        //Зчитуємо ID
        scanner = new Scanner(System.in);

        System.out.println("\nВведіть ID товару, який потрібно зчитати : ");

        boolean isntExists;
        //перевірити чи існує такий ID
        do{
            isntExists=false;
            newID=scanner.nextInt();
            newID=Math.abs(newID);
            //Перевіряємо чи такий ID існує
            isntExists = dbhelper.checkIdAlreadyExists(newID);
            if(isntExists)
                System.out.println("Такого ID не існує!Спробуйте ще раз");
        }while (isntExists);

        //Зчитуємо всі дані з файлу




        while(true) {

            //зчитуємо однин запис з файлу з індексом j
            line = FileWorker.read(getFilePath(), j);
            if(line.equals("+") || line.equals("-") || line.equals("")){
                j++;
                continue;
            }


            //дістали ID запису і порівнюємо його з іншими ID для сортування
            String IDline = line.split(",")[0];
            IDline.trim();
            lineID = Integer.parseInt(IDline);

            //якщо рівне - запамятовуємо індекс
            if (newID==lineID) {

                break;
            }

            j++;

        }
      String inputNewData = dbhelper.readInputFields(newID);



        while(true){

            line = FileWorker.read(getFilePath(), i);
            if(line.equals("nolines") && FileWorker.read(getFilePath(),i+1).equals("nolines")){
                break;
            }
            if(i==j){
                FileWorker.write(getOutputFilePath(), inputNewData);
            }
            else {
                FileWorker.write(getOutputFilePath(), line);
            }
i++;
        }

        dbhelper.delete();
        outputFile.renameTo(new File(dbhelper.DBPath,dbhelper.DBName));

        System.out.println("---------------Успішно відредаговано!-------------------\n");

    }














        public void delete() {

            File outputFile = dbhelper.createOutputFile();
            //Зчитуємо ID
            scanner = new Scanner(System.in);
            int id = -1, lineID, i = 0;
            String line;
            System.out.println("\nВведіть ID товару : ");

            boolean isntExists;
            //перевірити чи існує такий ID
            do {
                isntExists = false;
                id = scanner.nextInt();
                id = Math.abs(id);
                //Перевіряємо чи такий ID існує
                isntExists = dbhelper.checkIdAlreadyExists(id);
                if (isntExists)
                    System.out.println("Такого ID не існує!Спробуйте ще раз");
            } while (isntExists);


            while (true) {

                //зчитуємо однин запис з файлу з індексом j
                line = FileWorker.read(getFilePath(), i);
                if (line.equals("nolines") && FileWorker.read(getFilePath(), i + 1).equals("nolines")) {
                    break;
                }

                if(line.equals("+") || line.equals("-") || line.equals("")){
                    i++;
                    FileWorker.write(getOutputFilePath(), line);
                    continue;
                }
                //дістали ID запису і порівнюємо його з іншими ID для сортування
                String IDline = line.split(",")[0];
                IDline.trim();
                lineID = Integer.parseInt(IDline);

                //якщо рівне - запамятовуємо індекс
                if (lineID == id) {
                    i++;
                } else {
                    FileWorker.write(getOutputFilePath(), line);
                    i++;
                }


            }
            dbhelper.delete();
            outputFile.renameTo(new File(dbhelper.DBPath, dbhelper.DBName));

            int block = dbhelper.h(id);
            boolean overflow = checkBlockOverflow(block);
            i = 0;
            boolean BlockOverflow = false, inserted = false;

            if (overflow == true)
                return;
            else {

                while (true) {

                    //зчитуємо однин запис з файлу з індексом і
                    line = FileWorker.read(getFilePath(), i);
                    if (line.equals("nolines") && FileWorker.read(getFilePath(), i + 1).equals("nolines")) {
                        break;
                    }
                    if (line.equals("-")) {

                        BlockOverflow = true;
                        i++;
                        continue;
                    }

                    if (BlockOverflow) {

                        String IDline = line.split(",")[0];
                        IDline.trim();
                        lineID = Integer.parseInt(IDline);

                        //якщо це останній запис і вставка ще не відбулась то вставляємо запис і виходимо з циклу
                        if (dbhelper.h(lineID)==block) {

                            String InsertLine = line;

                            String lineid = InsertLine.split(",")[0];
                            lineid.trim();
                            int BlockOverflowID = Integer.parseInt(IDline);
                            i=0;
                            while (true) {

                                //зчитуємо однин запис з файлу з індексом j
                                line = FileWorker.read(getFilePath(), i);
                                if (line.equals("nolines") && FileWorker.read(getFilePath(), i + 1).equals("nolines")) {
                                    break;
                                }

                                if(line.equals("+") || line.equals("-") || line.equals("")){
                                    i++;
                                    FileWorker.write(getOutputFilePath(), line);
                                    continue;
                                }
                                //дістали ID запису і порівнюємо його з іншими ID для сортування
                                IDline = line.split(",")[0];
                                IDline.trim();
                                lineID = Integer.parseInt(IDline);

                                //якщо рівне - запамятовуємо індекс
                                if (lineID == BlockOverflowID) {
                                    i++;
                                }
                                else {
                                    FileWorker.write(getOutputFilePath(), line);
                                    i++;
                                }


                            }
                            dbhelper.delete();
                            outputFile.renameTo(new File(dbhelper.DBPath, dbhelper.DBName));
                            insert(InsertLine);
                            return;
                        }



                    }
                    i++;
                    line = "";

                }


                System.out.println("---------------Успішно видалено!-------------------\n");

            }
        }













        public String selectByID(){


            String line;
            int j=0,lineID,newID=-1;

            //Зчитуємо ID
            scanner = new Scanner(System.in);

            System.out.println("\nВведіть ID товару, для читання : ");

            boolean isntExists;
            //перевірити чи існує такий ID
            do{
                isntExists=false;
                newID=scanner.nextInt();
                newID=Math.abs(newID);
                //Перевіряємо чи такий ID існує
                isntExists = dbhelper.checkIdAlreadyExists(newID);
                if(isntExists)
                    System.out.println("Такого ID не існує!Спробуйте ще раз");
            }while (isntExists);

            //Зчитуємо всі дані з файлу




            while(true) {

                //зчитуємо однин запис з файлу з індексом j
                line = FileWorker.read(getFilePath(), j);
                if(line.equals("+") || line.equals("-") || line.equals("")){
                    j++;
                    continue;
                }


                //дістали ID запису і порівнюємо його з іншими ID для сортування
                String IDline = line.split(",")[0];
                IDline.trim();
                lineID = Integer.parseInt(IDline);

                //якщо рівне - запамятовуємо індекс
                if (newID==lineID) {
                    String[]s = line.split(",");
                    System.out.format(String.format(Printer.format, (Object[]) s));
                    break;
                }

                j++;

            }



            System.out.println("\n---------------Запис зчитано!-------------------\n");
            return line;

        }













    public void selectByField(){


        String line,fieldLine;
        int j=0,lineID,newID=-1;
        int IndexOffield;
        int counter=0;

        //Зчитуємо ID
        scanner = new Scanner(System.in);


        Printer.printChooseField();
        System.out.println("\nВведіть номер поля , яке потрібно зчитати : ");
        IndexOffield=Menu.getInput(1,10);
        System.out.println("\nВведіть значення поля , яке потрібно зчитати : ");
        fieldLine=scanner.nextLine();

        while(true) {

            //зчитуємо однин запис з файлу з індексом j
            line = FileWorker.read(getFilePath(), j);
            if(line.equals("nolines") && FileWorker.read(getFilePath(),j+1).equals("nolines")){
                System.out.println("\n\n@@@@@@@@   ЗНАЙДЕНО "+counter+" ЗАПИСІВ    @@@@@@@@");
                break;
            }

            if(line.equals("+") || line.equals("-") || line.equals("")){
                j++;
                continue;
            }

            //дістали ID запису і порівнюємо його з іншими ID для сортування
            String FieldFromFile = line.split(",")[IndexOffield-1];
            FieldFromFile.trim();

            //якщо рівне - запамятовуємо індекс
            if (FieldFromFile.equals(fieldLine)) {
                String[]s = line.split(",");
                System.out.format(String.format(Printer.format, (Object[]) s));
                counter++;
            }

            j++;

        }


    }


        public  String getFilePath(){
        return (new File(dbhelper.DBPath, dbhelper.DBName).getAbsolutePath());
        }

        public String getOutputFilePath(){
        return (new File(dbhelper.DBPath,"outputFile.txt").getAbsolutePath());
        }

    }











